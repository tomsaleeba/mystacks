# How to deploy
1. get a VM
1. install `docker` and `docker-compose`
1. git clone [this repo]
1. follow the instructions to [install cloudflared][0]
    ```bash
    wget -q https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64.deb
    dpkg -i cloudflared-linux-amd64.deb
    cloudflared tunnel login
    ```
1. `cd ./shiori-mystack`
1. `cp example.env .env`
1. `vim .env`
1. `docker-compose up -d`
1. setup a tunnel
    ```
    cloudflared tunnel create shioritunnel
    # get the UUID from
    cloudflared tunnel list
    cat << HEREDOC > /root/.cloudflared/shiori.yml
    url: http://localhost:8080
    tunnel: <Tunnel-UUID>
    credentials-file: /root/.cloudflared/<Tunnel-UUID>.json
    HEREDOC
    cloudflared tunnel --config /root/.cloudflared/shiori.yml route dns shioritunnel bookmarks.${yourdomain:?}.com
    cloudflared tunnel --config /root/.cloudflared/shiori.yml run shioritunnel
    cloudflared tunnel info shioritunnel
    ```
1. `cd ../miniflux-mystack`
1. `cp example.env .env`
1. `vim .env`
1. `docker-compose up -d`
1. setup a tunnel
    ```
    cloudflared tunnel create minifluxtunnel
    # get the UUID from
    cloudflared tunnel list
    cat << HEREDOC >> /root/.cloudflared/miniflux.yml
    url: http://localhost:8081
    tunnel: <Tunnel-UUID>
    credentials-file: /root/.cloudflared/<Tunnel-UUID>.json
    HEREDOC
    cloudflared tunnel --config /root/.cloudflared/miniflux.yml route dns minifluxtunnel reader.${yourdomain:?}.com
    cloudflared tunnel --config /root/.cloudflared/miniflux.yml run minifluxtunnel
    cloudflared tunnel info minifluxtunnel
    ```

Note: the tunnel runs in the foreground, so you have to leave it going in
screen/tmux/byobu.

[0]: https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/install-and-setup/tunnel-guide/#set-up-a-tunnel-locally-cli-setup
